### Fichier de configuration personnel de differents logiciel GNU/linux

Fichiers de configuration personnel pour i3wm et i3block

* Le fichier .Xresources contient une configaration pour urxvt-unicode

* Le dossier .font contient des fonts que justilise dans ma configuration de i3 pour le rendu du texte et les icones dans les Workspaces et i3block.

Fichier de conf sous licence WTFPL : [Do What the Fuck You Want to Public License](https://fr.wikipedia.org/wiki/WTFPL)
